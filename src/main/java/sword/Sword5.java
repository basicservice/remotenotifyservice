package sword;

public class Sword5 {

    public static void main(String[] args) {
        String s = "We are happy.";
        System.out.println(replaceSpace(s));
    }

    public static String replaceSpace(String s) {
        return s.replace(" ", "%20");
    }
}
