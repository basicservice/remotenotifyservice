package sword;

import static java.lang.Math.pow;

public class Sword14II {

    public static void main(String[] args) {
        int n = 120;
        System.out.println(cuttingRope(n));
    }

    public static int cuttingRope(int n) {
        if (n <= 3) {
            return n - 1;
        }
        int a = n / 3, b = n % 3;
        System.out.println("a:" + a + "  b:" + b);
        if (b == 0) {
            int res = (int) Math.pow(3, a);
            return res % 1000000007;
        }
        if (b == 1) {
            int res = (int) (Math.pow(3, a - 1) * 4);
            return res % 1000000007;
        }
        return (int) (Math.pow(3, a) * 2 % 1000000007);
    }
}
