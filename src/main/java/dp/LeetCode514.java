package dp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LeetCode514 {

    public static void main(String[] args) {
        String ring = "godding";
        String key = "godding";
        System.out.println(findRotateSteps1(ring, key));
    }

    public static int findRotateSteps1(String ring, String key) {
        int m = ring.length(), n = key.length();
        Map<Character, ArrayList<Integer>> charToIndex = new HashMap<>();
        for (int i = 0; i < ring.length(); i++) {
            char c = ring.charAt(i);
            charToIndex.putIfAbsent(c, new ArrayList<>());
            charToIndex.get(c).add(i);
        }
        //dp[i][j]表示圆盘指针在i,输入key[0...j]所需的最少操作数
        int[][] dp = new int[m + 1][n + 1];


        return dp[m][n];
    }

    /**
     * 字符->索引列表
     */
    static Map<Character, ArrayList<Integer>> charToIndex;
    static int[][] memo;

    /**
     * 返回最少操作步数
     * 每一次转动后，ring的字符串都会变
     *
     * @param ring
     * @param key
     * @return
     */
    public static int findRotateSteps(String ring, String key) {
        int m = ring.length();
        int n = key.length();
        charToIndex = new HashMap<>();
        memo = new int[m][n];
        //记录ring每个字符对应索引列表
        for (int i = 0; i < ring.length(); i++) {
            char c = ring.charAt(i);
            charToIndex.putIfAbsent(c, new ArrayList<>());
            charToIndex.get(c).add(i);
        }
        return dp(ring, 0, key, 0);
    }

    /**
     * 计算圆盘指针在ring[i]，输入key[j...]的最少操作数
     *
     * @param ring
     * @param i
     * @param key
     * @param j
     * @return
     */
    private static int dp(String ring, int i, String key, int j) {
        if (j == key.length()) {
            return 0;
        }
        if (memo[i][j] != 0) {
            return memo[i][j];
        }
        int n = ring.length();
        int res = Integer.MAX_VALUE;
        //ring上可能有多个字符key[j]
        for (int k : charToIndex.get(key.charAt(j))) {
            //波动指针的次数
            int delta = Math.abs(k - i);
            //选择顺指针还是逆指针
            delta = Math.min(delta, n - delta);
            int subProblem = dp(ring, k, key, j + 1);
            res = Math.min(res, 1 + delta + subProblem);
        }
        memo[i][j] = res;
        return res;
    }
}
