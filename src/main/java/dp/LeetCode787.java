package dp;

import java.util.*;

public class LeetCode787 {

    public static void main(String[] args) {
        int n = 3;
        int[][] flights = {
                {0, 1, 100},
                {1, 2, 100},
                {0, 2, 500}
        };
        int src = 0;
        int dst = 2;
        int k = 1;
        System.out.println(findCheapestPrice(n, flights, src, dst, k));

    }

    //记录每个节点的入度以及其对应的权值
    static Map<Integer, List<int[]>> indegree;
    static int srcp, dstp;
    static int[][] memo;

    /**
     * 返回从src出发，到达dst，最多途径k站的最低价格
     * 可以理解为加权图求最短路径的问题
     *
     * @param n       站点数
     * @param flights 边数
     * @param src     起点
     * @param dst     终点
     * @param k       限制边数
     * @return
     */
    public static int findCheapestPrice(int n, int[][] flights, int src, int dst, int k) {
        //将中转站个数转化为边数
        k++;
        srcp = src;
        dstp = dst;
        memo = new int[n][k + 1];
        for (int[] row : memo) {
            Arrays.fill(row, -2);
        }
        indegree = new HashMap<>();
        for (int[] flight : flights) {
            int from = flight[0];
            int to = flight[1];
            int price = flight[2];
            indegree.putIfAbsent(to, new ArrayList<>());
            indegree.get(to).add(new int[]{from, price});
        }
        return dp(dst, k);
    }


    /**
     * 从起点src出发，k步之内到达s的最小路径权重为dp(s,k)
     *
     * @param s
     * @param k
     * @return
     */
    private static int dp(int s, int k) {
        if (s == srcp) {
            return 0;
        }
        if (k == 0) {
            return -1;
        }
        if (memo[s][k] != -2) {
            return memo[s][k];
        }
        int res = Integer.MAX_VALUE;
        if (indegree.containsKey(s)) {
            for (int[] v : indegree.get(s)) {
                int from = v[0];
                int price = v[1];
                int subProblem = dp(from, k - 1);
                //无解的情况不用考虑
                if (subProblem != -1) {
                    res = Math.min(res, subProblem + price);
                }

            }
        }
        memo[s][k] = res == Integer.MAX_VALUE ? -1 : res;
        return memo[s][k];
    }
}
